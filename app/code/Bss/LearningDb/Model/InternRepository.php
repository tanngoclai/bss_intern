<?php

namespace Bss\LearningnDb\Model;

use Bss\LearningDb\Model\InternFactory;
use Bss\LearningnDb\Api\Data\InternInterface;
use Bss\LearningnDb\Api\InternRepositoryInterface;
use Bss\LearningnDb\Api\Data\InternSearchResultInterface;
use Bss\LearningnDb\Api\Data\InternSearchResultInterfaceFactory;
use Bss\LearningnDb\Model\ResourceModel\Intern\CollectionFactory as InternCollectionFactory;
use Bss\LearningnDb\Model\ResourceModel\Intern\Collection;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\NoSuchEntityException;

class InternRepository implements InternRepositoryInterface
{
    /**
     * @var InternFactory
     */
    private $internFactory;

    /**
     * @var InternCollectionFactory
     */
    private $internCollectionFactory;

    /**
     * @var InternSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * InternRepository constructor.
     * @param InternFactory $internFactory
     * @param InternCollectionFactory $internCollectionFactory
     * @param InternSearchResultInterfaceFactory $internSearchResultInterfaceFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        InternFactory $internFactory,
        InternCollectionFactory $internCollectionFactory,
        InternSearchResultInterfaceFactory $internSearchResultInterfaceFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->internFactory = $internFactory;
        $this->internCollectionFactory = $internCollectionFactory;
        $this->searchResultFactory = $internSearchResultInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * Save intern data.
     */
    public function save(InternInterface $intern)
    {
        $intern->getResource()->save($intern);
        if (!$intern) {
            throw new NoSuchEntityException(__('Could not save intern data'));
        }
        return $intern;
    }

    /**
     * Get intern data by id.
     */
    public function getById($id)
    {
        $intern = $this->internFactory->create();
        $intern->getResource()->load($intern, $id);
        if (!$intern->getId()) {
            throw new NoSuchEntityException(__('Unable to find intern with ID "%1"', $id));
        }
        return $intern;
    }

    /**
     * Get list intern data.
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->internCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * Delete intern.
     */
    public function delete(InternInterface $intern)
    {
        try {
            $intern->getResource()->delete($intern);
        } catch (NoSuchEntityException $e) {
            $e->getMessage();
        }
    }
}
