<?php
namespace Bss\LearningDb\Model\ResourceModel;

class Intern extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('internship', 'id');
    }
}
