<?php
namespace Bss\LearningDb\Model\ResourceModel\Intern;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Bss\LearningDb\Model\Intern', 'Bss\LearningDb\Model\ResourceModel\Intern');
    }

}
