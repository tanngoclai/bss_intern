<?php
namespace Bss\LearningDb\Model;

class Intern extends \Magento\Framework\Model\AbstractExtensibleModel implements \Bss\LearningDb\Api\Data\InternInterface
{
    const ID = 'id';
    const NAME = 'name';
    const AVATAR = 'avatar';
    const DATE_OF_BIRTH = 'dob';
    const DESCRIPTION = 'description';
    /**
     * Intern constructor.
     */
    protected function _construct()
    {
        $this->_init(\Bss\LearningDb\Model\ResourceModel\Intern::class);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param int $id
     * @return void
     */
    public function setId($id)
    {
        $this->setData(self::ID, $id);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->setData(self::NAME, $name);
    }

    /**
     * @return mixed
     */
    public function getAvatar(){
        return $this->getData(self::AVATAR);
    }

    /**
     * @param $link
     */
    public function setAvatar($link)
    {
        $this->setData(self::AVATAR, $link);
    }

    /**
     * @return mixed
     */
    public function getDateOfBirth(){
        return $this->getData(self::DATE_OF_BIRTH);
    }

    /**
     * @param $date
     */
    public function setDateOfBirth($date)
    {
        $this->setData(self::DATE_OF_BIRTH, $date);
    }

    /**
     * @return mixed
     */
    public function getDesciption(){
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @param $description
     */
    public function setDesciption($description)
    {
        $this->setData(self::DESCRIPTION, $description);
    }
}
