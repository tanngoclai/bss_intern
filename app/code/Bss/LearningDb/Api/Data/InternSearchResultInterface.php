<?php

namespace Bss\LearningDb\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface InternSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Bss\LearningDb\Api\Data\InternInterface[]
     */
    public function getItems();

    /**
     * @param \Bss\LearningDb\Api\Data\InternInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
