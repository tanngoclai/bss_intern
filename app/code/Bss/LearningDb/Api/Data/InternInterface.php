<?php

namespace Bss\LearningDb\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface InternInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return void
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return void
     */
    public function setName($name);

    /**
     * @return mixed
     */
    public function getAvatar();

    /**
     * @param string $link
     */
    public function setAvatar($link);

    /**
     * @return mixed
     */
    public function getDateOfBirth();

    /**
     * @param string $date
     */
    public function setDateOfBirth($date);

    /**
     * @return mixed
     */
    public function getDesciption();

    /**
     * @param string $description
     */
    public function setDesciption($description);
}
