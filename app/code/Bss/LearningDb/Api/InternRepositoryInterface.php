<?php

namespace Bss\LearningDb\Api;

use Bss\LearningDb\Api\Data\InternSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Bss\LearningDb\Api\Data\InternInterface;
use Magento\Framework\Exception\NoSuchEntityException;

interface InternRepositoryInterface
{

    /**
     * @param InternInterface $intern
     * @return InternInterface
     */
    public function save(InternInterface $intern);

    /**
     * @param int $id
     * @return InternInterface
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return InternSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param InternInterface $intern
     * @return void
     */
    public function delete(InternInterface $intern);

}
