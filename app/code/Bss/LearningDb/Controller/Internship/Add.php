<?php

namespace Bss\LearningDb\Controller\Internship;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Add extends Action
{
    /**
     *
     * @return void
     */

    protected $intern;
    public function __construct(Context $context, \Bss\LearningDb\Model\InternFactory $intern)
    {
        $this->intern = $intern;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = (array) $this->getRequest()->getParams();

        $internship = $this->intern->create();

        if (!empty($post)) {
            $internship->setData($post);
            $internship->save();

            // Display the succes form validation message
            $this->messageManager->addSuccessMessage('Done !');

            // Redirect to your form page (or anywhere you want...)
            return $this->_redirect('db/internship/index');
        }
    }
}
