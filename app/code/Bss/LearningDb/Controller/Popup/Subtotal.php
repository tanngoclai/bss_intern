<?php

namespace Bss\LearningDb\Controller\Popup;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Checkout\Model\Cart;

class Subtotal extends Action
{
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    /**
     * @var Cart
     */
    private $cart;

    /**
     * Index constructor.
     * @param Context $context
     * @param JsonFactory $jsonResultFactory
     * @param Cart $cart
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonResultFactory,
        Cart $cart
    ) {
        $this->jsonResultFactory = $jsonResultFactory;
        $this->cart = $cart;
        parent::__construct($context);
    }

    /**
     *
     * @return Json
     */
    public function execute()
    {
        $data = ['subtotal' => $this->cart->getQuote()->getSubtotal()];
        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
    }
}
