<?php
namespace Bss\learningDb\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Show extends Template
{
    protected $_internFactory;
    protected $_data;
    public function __construct(
        Context $context,
        \Bss\LearningDb\Model\InternFactory $internFactory
    )
    {
        $this->_internFactory = $internFactory;
        parent::__construct($context);
    }

    public function getShowData(){
        $result = $this->_internFactory->create();
        $id = $this->getRequest()->getParams('id');
        $this->_data=$result->load($id);
        return $this->_data;
    }
}
