<?php
namespace Bss\learningDb\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Index extends Template
{
    protected $_internFactory;
    protected $_data;
    public function __construct(
        Context $context,
        \Bss\LearningDb\Model\InternFactory $internFactory
    )
    {
        $this->_internFactory = $internFactory;
        parent::__construct($context);
    }

    public function getListData(){
        $intern = $this->_internFactory->create();
        return $intern->getCollection();
    }
}
