<?php
namespace Bss\LearningDb\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;

class CheckLoginEvent implements ObserverInterface
{
    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ManagerInterface
     */
    public $messageManager;

    /**
     * CheckLoginEvent constructor.
     * @param UrlInterface $url
     * @param Session $session
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        UrlInterface $url,
        Session $session,
        ManagerInterface $messageManager
    ) {
        $this->session = $session;
        $this->url = $url;
        $this->messageManager = $messageManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if (!$this->session->isLoggedIn()) {
            $this->messageManager->addErrorMessage('You have to login');
            $customerBeforeAuthUrl = $this->url->getUrl('customer/account/login');
            $observer->getControllerAction()->getResponse()->setRedirect($customerBeforeAuthUrl);
        }
    }
}
