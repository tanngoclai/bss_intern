<?php
namespace Bss\LearningDb\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;

class NamePage implements ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    public $messageManager;

    /**
     * CheckLoginEvent constructor.
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        ManagerInterface $messageManager
    ) {

        $this->messageManager = $messageManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $cmsName = $observer->getEvent()->getPage()->getTitle();
        $this->messageManager->addNoticeMessage('Name of page: '.(string)$cmsName);
    }
}
