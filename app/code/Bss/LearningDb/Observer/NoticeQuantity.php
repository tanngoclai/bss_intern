<?php
namespace Bss\LearningDb\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\CatalogInventory\Model\Stock\StockItemRepository as StockItem;

class NoticeQuantity implements ObserverInterface
{
    /**
     * @var StockItem
     */
    protected $stockItem;

    /**
     * @var ManagerInterface
     */
    public $messageManager;

    /**
     * NoticeQuantity constructor.
     * @param StockItem $stockItem
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        StockItem $stockItem,
        ManagerInterface $messageManager
    ) {
        $this->stockItem = $stockItem;
        $this->messageManager = $messageManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $productQtyInStock = $this->stockItem->get($product->getId())->getQty();
        $productQtyInCart = $product->getQty();
        $productQty = $productQtyInStock - $productQtyInCart;
        if ($productQty >= 0) {
            $this->messageManager->addNoticeMessage(' quantity in stock of '.$product->getName().' is ' . (string)$productQty);
        } else {
            throw new NoSuchEntityException(__(' '.$product->getName().' was out of stock'));
        }
    }
}
