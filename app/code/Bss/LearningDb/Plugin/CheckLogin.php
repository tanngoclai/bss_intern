<?php

namespace Bss\LearningDb\Plugin;

use Magento\Framework\Message\ManagerInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\Response\Http as responseHttp;
use Magento\Catalog\Controller\Product\View;


class CheckLogin
{
    /**
     * @var UrlInterface
     */
    public $url;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var ManagerInterface
     */
    public $messageManager;

    /**
     * @var responseHttp 
     */
    protected $response;

    /**
     * CheckLogin constructor.
     * @param responseHttp $response
     * @param UrlInterface $url
     * @param Session $session
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        responseHttp $response,
        UrlInterface $url,
        Session $session,
        ManagerInterface $messageManager
    ) {
        $this->response = $response;
        $this->session = $session;
        $this->url = $url;
        $this->messageManager = $messageManager;
    }

    /**
     * @param View $view
     */
    public function beforeExecute(View $view)
    {
        if (!$this->session->isLoggedIn()) {
            $this->messageManager->addErrorMessage('You have to login');
            $url = $this->url->getUrl('customer/account/login');
            $this->response->setRedirect($url);
        }
    }
}
