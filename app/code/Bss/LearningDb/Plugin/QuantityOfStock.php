<?php

namespace Bss\LearningDb\Plugin;

use Magento\Checkout\Model\Cart;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\CatalogInventory\Model\Stock\StockItemRepository as StockItem;

class QuantityOfStock
{
    /**
     * @var StockItem
     */
    protected $stockItem;

    /**
     * @var ManagerInterface
     */
    public $messageManager;

    /**
     * QuantityOfStock constructor.
     * @param ManagerInterface $messageManager
     * @param StockItem $stockItem
     */
    public function __construct(
        ManagerInterface $messageManager,
        StockItem $stockItem
    ) {
        $this->messageManager = $messageManager;
        $this->stockItem = $stockItem;
    }

    /**
     * @param Cart $result
     * @param $productInfo
     * @param null $requestInfo
     * @return Cart
     * @throws NoSuchEntityException
     */
    public function afterAddProduct(Cart $result, $productInfo, $requestInfo = null)
    {
        $productId = $requestInfo->getEntityId();
        $productQtyInStock = $this->stockItem->get($productId)->getQty();
        $productQtyInCart = $result->getQuote()->getItemsQty();
        $productQty = $productQtyInStock - $productQtyInCart;
        if ($productQty >= 0) {
            $this->messageManager->addNoticeMessage(' quantity in stock of this product is ' . (string)$productQty);
        } else {
            throw new NoSuchEntityException(__('This product was out of stock'));
        }
        return $result;
    }
}
