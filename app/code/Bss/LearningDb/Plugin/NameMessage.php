<?php

namespace Bss\LearningDb\Plugin;

use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Message\MessageInterface;
use Magento\Customer\Model\Session;

class NameMessage
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * NameMessage constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param ManagerInterface $subject
     * @param $result
     * @return string[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeAddMessage(ManagerInterface $subject, MessageInterface $message, $group=null)
    {
        $text = $message->getText();
        if ($this->session->isLoggedIn()) {
            $text = 'Dear '.$this->session->getCustomer()->getName().', '.$text;

        } else {
            $text = 'Dear guest, ' . $text;
        }
        $message->setText($text);
        return [$message,$group];
    }
}
