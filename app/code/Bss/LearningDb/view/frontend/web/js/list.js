define([
    'jquery'
], function ($) {
    'use strict';
    $.widget('bss.internshipList', {
        options: {},
        /**
         * @private
         */
        _create: function () {
            this.element.owlCarousel(this.options);
        },
    });

    return $.bss.internshipList;
});
