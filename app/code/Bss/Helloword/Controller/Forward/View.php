<?php
namespace Bss\Helloword\Controller\Forward;

use \Magento\Framework\App\Action\Action;

class View extends Action
{

    public function execute()
    {
        return $this->_forward('index','index','cms');
    }
}
