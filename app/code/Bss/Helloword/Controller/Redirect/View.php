<?php
namespace Bss\Helloword\Controller\Redirect;

use Magento\Framework\App\Action\Context;
use \Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Store\Model\StoreManagerInterface;

class View extends Action
{
    protected $redirectFactory;
    protected $storeManager;

    public function __construct(
        RedirectFactory $redirectFactory,
        StoreManagerInterface $storeManager,
        Context $context
    ) {
        $this->redirectFactory = $redirectFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    public function execute()
    {
        $redirect = $this->redirectFactory->create();

        $url = $this->storeManager->getStore()->getBaseUrl();

        $redirect->setUrl($url);

        return $redirect;
    }
}

