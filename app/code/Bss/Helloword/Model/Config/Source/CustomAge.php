<?php

namespace Bss\Helloword\Model\Config\Source;

class CustomAge implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Return array of options as value-label pairs, eg. value => label
     *
     * @return array
     */
    public function toOptionArray()
    {
        $age = [];
        for ($i = 1; $i<100; $i++) {
            $age[] = ['value' => $i, 'label' => (string)$i];
        }
        return $age;
    }
}
