<?php

namespace Bss\Helloword\Model\Config\Backend;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class SetAge extends \Magento\Framework\App\Config\Value
{
    /**
     *  @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $configWriter;

    /**
     * @param WriterInterface $configWriter
     */

    public function __construct(
        WriterInterface $configWriter,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
        $this->configWriter = $configWriter;
    }

    public function beforeSave()
    {
        $date_of_birth = $this->getValue();
        $birth_year = (int) substr($date_of_birth,-4,4);
        $birth_month = (int) substr($date_of_birth,3,2);
        $birth_day = (int) substr($date_of_birth,0,2);
        $year = (int) date('Y');
        $month = (int) date('m');
        $day = (int) date('d');

        $age = $year - $birth_year;
        if ($month < $birth_month) {
            $age--;
        } elseif ($month == $birth_month && $day < $birth_day) {
            $age--;
        }

        if ($age >= 1 && $age <= 99) {
            $this->configWriter->save('helloword/general/age', $age, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeId = 0);
        } else {
            throw new \Magento\Framework\Exception\ValidatorException(__('Age is invalid.'));
        }
    }
}
