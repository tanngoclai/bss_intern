<?php

namespace Bss\Helloword\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;
    private $eavConfig;

    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,
                'custom_attribute',
                [
                    'group' => 'General',
                    'attribute_set_id' => 'Bag',
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Custom Attribute',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => 0,
                    'searchable' => true,
                    'filterable' => true,
                    'comparable' => true,
                    'visible_on_front' => true,
                    'sort_order' => 50,
                    'option' => [
                        'values' => [],
                    ]
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.0.12', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY,
                'custom_customer_attribute',
                [
                    'type' => 'varchar',
                    'label' => 'Tanln add new customer attribute ',
                    'input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'source' => '',
                    'backend' => '',
                    'user_defined' => false,
                    'is_user_defined' => false,
                    'sort_order' => 1000,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'is_searchable_in_grid' => false,
                    'position' => 1000,
                    'default' => 0,
                    'system' => 0
                ]
            );
        }
    }
}
