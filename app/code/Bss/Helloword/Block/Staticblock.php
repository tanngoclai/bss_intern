<?php
namespace Bss\Helloword\Block;

use Magento\Framework\View\Element\Template;

class Staticblock extends Template
{
    public function sayHello()
    {
       return __('Hello, My name ís "static block"');
    }
}
