<?php
namespace Bss\Helloword\Block;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Loadcategory extends Template
{

    protected $categoryFactory;

    public function __construct(
        Context $context,
        CategoryFactory $categoryFactory
    ) {
        $this->categoryFactory = $categoryFactory;
        parent::__construct($context);
    }
    public function getCategory()
    {
        $category = $this->categoryFactory->create()->load('3');
        return $category;
    }
    public function getProductCollection()
    {
        return $this->getCategory()->getProductCollection()
                    ->setPageSize(5)
                    ->setCurPage(1)
                    ->addAttributeToSelect('*');
    }
}
